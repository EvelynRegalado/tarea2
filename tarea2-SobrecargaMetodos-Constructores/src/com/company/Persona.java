package com.company;

/**
 * Created by Pc Evelyn on 15/05/2017.
 */
public class Persona {
    public String nombre;
    public int edad;

    //Constructor1
    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    //Contructor2
    public Persona(String nombre) {
        this.nombre = nombre;
    }
    //Contructor3
    public Persona(int edad) {
        this.edad = edad;
    }
    //Constructor4
    public Persona(){

    }
    //Sobrecarga de Métodos
    //METODO-1
    public void unMetodo(int i){
        System.out.println("Metodo con argumento entero");
    }
    //METODO-2
    public void unMetodo(float i){
        System.out.println("Metodo con argumento flotante");
    }

}
