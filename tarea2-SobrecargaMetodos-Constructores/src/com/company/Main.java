package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Persona p1 =new Persona();
        Persona p2 =new Persona("Evelyn");
        Persona p3 =new Persona(20);
        Persona p4 =new Persona("Evelyn", 20);

        System.out.println("Nombre: " + p1.nombre + ", edad: " + p1.edad);
        System.out.println("Nombre: " + p2.nombre + ", edad: " + p2.edad);
        System.out.println("Nombre: " + p3.nombre + ", edad: " + p3.edad);
        System.out.println("Nombre: " + p4.nombre + ", edad: " + p4.edad);

        p1.unMetodo(16);
        p1.unMetodo(16.5f);
    }
}
