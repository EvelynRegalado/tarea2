package com.company;

/**
 * Created by Evelyn on 15/05/2017.
 */
public class Auto implements Vehiculos {
    int velocidad=0;

    @Override
    public String frenar(int valor) {
        velocidad -=valor;
        return "El auto freno y va a: "+velocidad+"km/h";
    }

    @Override
    public String acelerar(int valor) {
        String cadena="";
        velocidad+=valor;

        if (velocidad>Velo_Max)
            cadena="ALERTA!! Exceso de velocidad"+"\n";
        cadena+="El auto esta acelerando y va a: "+velocidad+"km/h";
        return cadena;
    }
    public int plazas(){
        return 4;

    }
}
