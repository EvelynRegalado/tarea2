package com.company;

/**
 * Created by Evelyn on 15/05/2017.
 */
public class Moto implements Vehiculos{
    int velocidad;

    @Override
    public String frenar(int valor) {
        velocidad -=valor;
        return "La moto freno y va a: "+velocidad+"km/h";
    }

    @Override
    public String acelerar(int valor) {
        String cadena="";
        velocidad+=valor;

        if (velocidad>Velo_Max)
            cadena="ALERTA!! Exceso de velocidad";
        cadena+="La moto esta acelerando y va a: "+velocidad+"km/h";
        return cadena;
    }
    public int plazas(){
        return 2;

    }
}

