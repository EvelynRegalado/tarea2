package com.company;

/**
 * Created by Evelyn on 15/05/2017.
 */

//Creacion de la clase hijo
public class Esfera extends Circulo {
    /* double radio; */

    //Sobreescritura del método del padre
    public void calculo(){
        System.out.println("El cálculo del area de la esfera es: "+ 4*ValorPi*(radio*radio));
    }
}
