package com.company;

/**
 * Created by Evelyn on 17/05/2017.
 */
public class Clase {
    //Se utilizara un atributo de tipo private
    //Se generara los metodos get y set
    //Los metodos debes ser publicos para forzar el ecceso a las variables

    private int tipo;

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}

